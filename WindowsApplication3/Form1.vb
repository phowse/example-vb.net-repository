﻿Public Class Form1

    Dim arrPic(35) As PictureBox

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i As Integer
        'For i = 0 To 8

        For x = 0 To 5
            For y = 0 To 5

                Dim steve As New PictureBox
                steve.BackColor = Color.Fuchsia
                steve.Top = 10 + 50 * x
                steve.Left = 10 + 50 * y
                steve.Width = 50
                steve.Height = 50
                steve.BorderStyle = BorderStyle.FixedSingle
                Me.Controls.Add(steve)
                arrPic(i) = steve
                i += 1

            Next
        Next
        'Next

    End Sub

    Private Function getRand(max As Integer)
        Dim x As Integer
        x = Int(Rnd() * max) + 1
        Return x
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        
        For i = 0 To 35

            arrPic(i).BackColor = Color.FromArgb(getRand(255), getRand(255), getRand(255))
            arrPic(i).Left += 5 - getRand(9)
            arrPic(i).Top += 5 - getRand(9)
            arrPic(i).Height += 5 - getRand(9)
            arrPic(i).Width += 5 - getRand(9)

        Next

    End Sub
End Class
